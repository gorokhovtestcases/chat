<?php

declare(strict_types=1);

namespace Chat\Domain\Messenger;

class Chat
{
    public function __construct(
        private ChatId $chatId,
        private MessageList $messageList,
    ){}

    public function getChatId(): ChatId
    {
        return $this->chatId;
    }

    public function getMessageList(): MessageList
    {
        return $this->messageList;
    }
}