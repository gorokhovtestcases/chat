<?php

declare(strict_types=1);

namespace Chat\Domain\Messenger;

interface ChatRepository
{
    public function getById(ChatId $id): Chat;
}