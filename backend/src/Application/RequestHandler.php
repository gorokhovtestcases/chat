<?php

declare(strict_types=1);

namespace Chat\Application;

interface RequestHandler
{
    public function supports(Request $request): bool;
    public function handle(Request $request): Response;
}