<?php

declare(strict_types=1);

namespace Chat\Application;

use Chat\Application\Messenger\MessageListRequest;
use OpenSwoole\Http\Request;

class ApplicationRequestFactory
{
    public function __construct() {}

    public function makeMessageListRequest(Request $request): MessageListRequest
    {
        return new MessageListRequest($request->header);
    }
}