<?php

namespace Chat\Application;

interface Request
{
    public function getHeaders(): array;
    public function getContent(): ?ApplicationRequestDTO;
}