<?php

declare(strict_types=1);

namespace Chat\Application;

abstract class ApplicationRequest implements Request
{
    public function __construct(
        private readonly array $headers,
        private readonly ?ApplicationRequestDTO $requestDTO = null,
    ) {}

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getContent(): ?ApplicationRequestDTO
    {
        return $this->requestDTO;
    }
}