<?php

declare(strict_types=1);

namespace Chat\Application\Exception;

use LogicException;

class ApplicationException extends LogicException
{

}