<?php

declare(strict_types=1);

namespace Chat\Application;

use Chat\Application\Exception\ApplicationException;
use Chat\Application\Messenger\MessageListRequest;
use Chat\Application\Messenger\MessageListRequestHandler;
use DI\Attribute\Inject;

class Application
{
    private array $handlerList = [];

    public function __construct (
        #[Inject] private readonly MessageListRequestHandler $messageListRequestHandler
    ) {
        $this->handlerList = [
            $this->messageListRequestHandler,
        ];
    }

    public function handle(ApplicationRequest $request): Response
    {
        foreach ($this->handlerList as $handler) {
            if (!$handler->supports($request)) {
                continue;
            }

            return $handler->handle($request);
        }

        throw new ApplicationException("Unhandled request " . get_class($request));
    }
}