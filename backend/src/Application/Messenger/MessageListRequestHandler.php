<?php

declare(strict_types=1);

namespace Chat\Application\Messenger;

use Chat\Application\ApplicationRequest;
use Chat\Application\ApplicationResponse;
use Chat\Application\Request;
use Chat\Application\RequestHandler;
use Chat\Application\Response;

class MessageListRequestHandler implements RequestHandler
{
    /**
     * @param MessageListRequest $request
     */
    public function handle(Request $request): Response
    {
        var_dump($request);die();
    }

    public function supports(Request $request): bool
    {
        return $request instanceof MessageListRequest;
    }
}