<?php

require_once __DIR__ . "/vendor/autoload.php";

use OpenSwoole\WebSocket\Server;
use OpenSwoole\Http\Request;
use OpenSwoole\WebSocket\Frame;
use Chat\Application\Application;
use \Chat\Application\ApplicationRequestFactory;

$container = new DI\Container();

$app = $container->get(Application::class);
$server = new Server("0.0.0.0", 8895);

$server->on(
    "Start",
    function(Server $server) {
        echo "OpenSwoole WebSocket Server is started at http://0.0.0.0:8895\n";
    }
);

$server->on(
    'Open',
    function(Server $server, Request $request) use ($app, $container) {
        $response = $app->handle(
            $container
                ->get(ApplicationRequestFactory::class)
                ->makeMessageListRequest($request)
        );
        $server->push($request->fd, json_encode(["Open", time()]));
    }
);

$server->on(
    'Message',
    function(Server $server, Frame $frame) {
        echo "received message: {$frame->data}\n";
        $server->push($frame->fd, json_encode(["Message", time()]));
    }
);

$server->on(
    'Close',
    function(Server $server, int $fd) {
        echo "connection close: {$fd}\n";
    }
);

$server->on(
    'Disconnect',
    function(Server $server, int $fd) {
        echo "connection disconnect: {$fd}\n";
    }
);

$server->start();