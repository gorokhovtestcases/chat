import React from 'react';
import {Button, Input, MessageList,} from 'react-chat-elements'

import 'react-chat-elements/dist/main.css'
import { Col, Layout, Row } from "antd";

function App() {
  return (
      <Layout>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={8}>
              </Col>
              <Col span={8}>
                  <Input
                      placeholder="Type here..."
                      multiline={true}
                      maxHeight={1000}
                  />
              </Col>
              <Col span={8}>
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row align="middle">
              <Col span={8}>
              </Col>
              <Col span={8}>
                  <MessageList
                      className='message-list'
                      lockable={true}
                      toBottomHeight={'100%'}
                      dataSource={[
                          {
                              id: "12123123",
                              position: "left",
                              text: "Text1",
                              title: "Title1",
                              focus: false,
                              date: new Date(),
                              titleColor: '#bcbcbc',
                              forwarded: false,
                              replyButton: false,
                              removeButton: false,
                              status: 'waiting',
                              notch: false,
                              retracted: false,
                              type: 'text'
                          },
                          {
                              id: "12123123",
                              position: "right",
                              text: "Text1",
                              title: "Title1",
                              focus: false,
                              date: new Date(),
                              titleColor: '#bcbcbc',
                              forwarded: false,
                              replyButton: false,
                              removeButton: false,
                              status: 'waiting',
                              notch: false,
                              retracted: false,
                              type: 'text'
                          },
                          {
                              id: "12123123",
                              position: "right",
                              text: "Text1",
                              title: "Title1",
                              focus: false,
                              date: new Date(),
                              titleColor: 'red',
                              forwarded: false,
                              replyButton: false,
                              removeButton: false,
                              status: 'read',
                              notch: false,
                              retracted: false,
                              type: 'text'
                          },
                          {
                              id: "12123123",
                              position: "left",
                              text: "Text1",
                              title: "Title1",
                              focus: false,
                              date: new Date(),
                              titleColor: '#bcbcbc',
                              forwarded: false,
                              replyButton: false,
                              removeButton: false,
                              status: 'waiting',
                              notch: false,
                              retracted: false,
                              type: 'text'
                          },
                       ]}
                   referance={{}}/>
              </Col>
              <Col span={8}>
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
          <Row>
              <Col span={24}>
                  &nbsp;
              </Col>
          </Row>
      </Layout>
  );
}

export default App;